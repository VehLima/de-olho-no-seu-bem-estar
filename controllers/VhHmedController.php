<?php

namespace app\controllers;

use Yii;
use app\models\VhHmed;
use app\models\VhHmedSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * VhHmedController implements the CRUD actions for VhHmed model.
 */
class VhHmedController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all VhHmed models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VhHmedSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single VhHmed model.
     * @param integer $id_usuario
     * @param integer $id_cons
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id_usuario, $id_cons)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_usuario, $id_cons),
        ]);
    }

    /**
     * Creates a new VhHmed model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new VhHmed();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_usuario' => $model->id_usuario, 'id_cons' => $model->id_cons]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing VhHmed model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id_usuario
     * @param integer $id_cons
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id_usuario, $id_cons)
    {
        $model = $this->findModel($id_usuario, $id_cons);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_usuario' => $model->id_usuario, 'id_cons' => $model->id_cons]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing VhHmed model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id_usuario
     * @param integer $id_cons
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id_usuario, $id_cons)
    {
        $this->findModel($id_usuario, $id_cons)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the VhHmed model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id_usuario
     * @param integer $id_cons
     * @return VhHmed the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_usuario, $id_cons)
    {
        if (($model = VhHmed::findOne(['id_usuario' => $id_usuario, 'id_cons' => $id_cons])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
