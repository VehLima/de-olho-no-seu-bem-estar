<?php
 
namespace app\controllers;
use Yii;
use yii\data\SqlDataProvider;
 
class RelatoriosController extends \yii\web\Controller
{
   public function actionIndex()
   {
       return $this->render('index');
   }
 


public function actionRelatorio1()
   {
       $consulta = new SqlDataProvider([
        'sql' => 'SELECT VH_USUARIO.NOME, COUNT(VH_CONSULTAS.ID_CONS)AS QUANTIDADE
        FROM VH_USUARIO JOIN VH_CONSULTAS ON VH_USUARIO.ID_USUARIO=VH_CONSULTAS.ID_USUARIO 
        GROUP BY  VH_USUARIO.ID_USUARIO
        ORDER BY COUNT(VH_CONSULTAS.ID_CONS) DESC',
            ]
        );
        
        return $this->render('relatorio1', ['resultado' => $consulta]);
   }

public function actionRelatorio2()
   {
    $consulta = new SqlDataProvider([
     'sql' => 'SELECT VH_USUARIO.NOME, COUNT(VH_ALIMENTACAO.ID_ALIMENT)AS QUANTIDADE
     FROM VH_USUARIO JOIN VH_ALIMENTACAO ON VH_USUARIO.ID_USUARIO=VH_ALIMENTACAO.ID_USUARIO
     GROUP BY  VH_USUARIO.ID_USUARIO
     ORDER BY COUNT(VH_ALIMENTACAO.ID_ALIMENT) DESC',
         ]
     );
     
     return $this->render('relatorio2', ['resultado' => $consulta]);
}

public function actionRelatorio3()
   {
    $consulta = new SqlDataProvider([
     'sql' => 'SELECT VH_USUARIO.NOME,MONTH(DIA),COUNT(*) AS QUANTIDADE
     FROM VH_USUARIO JOIN VH_CONSULTAS ON VH_USUARIO.ID_USUARIO=VH_CONSULTAS.ID_USUARIO
     GROUP BY  VH_USUARIO.ID_USUARIO, MONTH(dia)
     ORDER BY VH_USUARIO.NOME,MONTH(DIA)',
         ]
     );
     
     return $this->render('relatorio3', ['resultado' => $consulta]);
}

public function actionRelatorio4()
   {
    $consulta = new SqlDataProvider([
     'sql' => 'SELECT COUNT(nome) AS QUANTIDADE
     FROM VH_MEDICAMENTO ',
         ]
     );
     
     return $this->render('relatorio4', ['resultado' => $consulta]);
}


public function actionRelatorio5()
   {
    $consulta = new SqlDataProvider([
        'sql' => 'SELECT VH_USUARIO.NOME,DAY(DIA),COUNT(*) AS QUANTIDADE
        FROM VH_USUARIO JOIN VH_CONSULTAS ON VH_USUARIO.ID_USUARIO=VH_CONSULTAS.ID_USUARIO
        GROUP BY  VH_USUARIO.ID_USUARIO, DAY(dia)
        ORDER BY VH_USUARIO.NOME,DAY(DIA)',
            ]
     );
     
     return $this->render('relatorio5', ['resultado' => $consulta]);
}





}


?>
