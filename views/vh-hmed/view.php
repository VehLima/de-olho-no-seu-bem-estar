<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\VhHmed */

$this->title = $model->id_usuario;
$this->params['breadcrumbs'][] = ['label' => 'Informações', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="vh-hmed-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Atualizar', ['update', 'id_usuario' => $model->id_usuario, 'id_cons' => $model->id_cons], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Apagar', ['delete', 'id_usuario' => $model->id_usuario, 'id_cons' => $model->id_cons], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Tem certeza de que deseja excluir este item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_usuario',
            'descricao',
            'id_cons',
        ],
    ]) ?>

</div>
