<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\VhHmed */

$this->title = 'Atualizar Informações: ' . $model->id_usuario;
$this->params['breadcrumbs'][] = ['label' => 'Informações', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_usuario, 'url' => ['view', 'id_usuario' => $model->id_usuario, 'id_cons' => $model->id_cons]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="vh-hmed-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
