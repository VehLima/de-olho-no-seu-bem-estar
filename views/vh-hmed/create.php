<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\VhHmed */

$this->title = 'Consultar Informações';
$this->params['breadcrumbs'][] = ['label' => 'Informações', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vh-hmed-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
