<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\VhHmedSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Informações';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vh-hmed-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Adicionar Informações', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'descricao',
            ['attribute'=>'id_usuario.nome','label'=>'Usuário'],
            ['attribute'=>'id_cons.descricao','label'=>'Consultas'],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
