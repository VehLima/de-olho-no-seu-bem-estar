<?php
 
use yii\helpers\Html;
 
$this->title = 'Relatórios';
$this->params['breadcrumbs'][] = $this->title;
?>
 
<div class="relatorios-index">
 
   <h1><?= Html::encode($this->title) ?></h1>


   <?= Html::a('Quantidade de consultas por usuário', ['relatorio1'], ['class' => 'btn btn-success']) ?> <br> <br>
   <?= Html::a(' Alimentação por usuário', ['relatorio2'], ['class' => 'btn btn-success']) ?> <br> <br>
   <?= Html::a('Quantidade de consultas por Mês', ['relatorio3'], ['class' => 'btn btn-success']) ?> <br> <br>
   <?= Html::a('Quantidade de medicamento em uso', ['relatorio4'], ['class' => 'btn btn-success']) ?> <br> <br>
   <?= Html::a('Quantidade de consultas por dia', ['relatorio5'], ['class' => 'btn btn-success']) ?> <br> <br>
 
</div>
