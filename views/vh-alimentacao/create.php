<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\VhAlimentacao */

$this->title = 'Adicionar Alimentação';
$this->params['breadcrumbs'][] = ['label' => 'Alimentaçãos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vh-alimentacao-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
