<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\VhAlimentacaoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vh-alimentacao-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'horário') ?>

    <?= $form->field($model, 'descricao') ?>

    <?= $form->field($model, 'id_usuario') ?>

    <?= $form->field($model, 'id_aliment') ?>

    <div class="form-group">
        <?= Html::submitButton('Pesquisar', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Restabelecer', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
