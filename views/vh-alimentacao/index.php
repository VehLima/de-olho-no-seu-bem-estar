<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\VhAlimentacaoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Alimentações';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vh-alimentacao-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Adicionar Alimentação', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'horário',
            'descricao',
            ['attribute'=>'id_usuario.nome','label'=>'Usuário'],
            ['attribute'=>'id_aliment.descricao','label'=>'Alimentação'],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
