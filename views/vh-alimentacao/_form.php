<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\VhUsuario;


/* @var $this yii\web\View */
/* @var $model app\models\VhAlimentacao */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vh-alimentacao-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'horário')->textInput(['type'=>'time']) ?>

    <?= $form->field($model, 'descricao')->textInput(['maxlength' => true]) ?>
  
    <?= $form->field($model, 'id_usuario')->
       dropDownList(ArrayHelper::map(VhUsuario::find()
           ->orderBy('nome')
           ->all(),'id_usuario','nome'),
           ['prompt' => 'Selecione um usuário'] )
?>


    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
