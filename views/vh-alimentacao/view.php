<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\VhAlimentacao */

$this->title = $model->id_usuario;
$this->params['breadcrumbs'][] = ['label' => 'Alimentações', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="vh-alimentacao-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Atualizar', ['update', 'id_usuario' => $model->id_usuario, 'id_aliment' => $model->id_aliment], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Apagar', ['delete', 'id_usuario' => $model->id_usuario, 'id_aliment' => $model->id_aliment], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Tem certeza de que deseja excluir este item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'horário',
            'descricao',
            
            ['attribute'=>'id_usuario.nome', 'label'=>'Codigo do Usuário'],
            ['attribute'=>'id_aliment.descricao', 'label'=>'Codigo da Alimentação'],

        ],
    ]) ?>

</div>
