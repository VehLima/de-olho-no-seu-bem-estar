<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\VhAlimentacao */

$this->title = 'Atualizar Alimentação: ' . $model->id_usuario;
$this->params['breadcrumbs'][] = ['label' => 'Alimentaçãos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_usuario, 'url' => ['view', 'id_usuario' => $model->id_usuario, 'id_aliment' => $model->id_aliment]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="vh-alimentacao-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
