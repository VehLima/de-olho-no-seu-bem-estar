<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\VhUsuario;

/* @var $this yii\web\View */
/* @var $model app\models\VhConsultas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vh-consultas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dia')->textInput(['type'=>'date'])?>

    <?= $form->field($model, 'horário')->textInput(['type'=>'time'])?>

    <?= $form->field($model, 'local')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_usuario')->
       dropDownList(ArrayHelper::map(VhUsuario::find()
           ->orderBy('nome')
           ->all(),'id_usuario','nome'),
           ['prompt' => 'Selecione um usuário'] )
?>


    <?= $form->field($model, 'descricao')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
