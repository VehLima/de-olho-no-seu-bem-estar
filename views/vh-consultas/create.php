<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\VhConsultas */

$this->title = 'Adicionar Consultas';
$this->params['breadcrumbs'][] = ['label' => 'Consultas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vh-consultas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
