<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\VhConsultasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Consultas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vh-consultas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Adicionar Consultas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute'=>'dia',
                'filterInputOptions' => [
                    'type'=>'date',
                ],
            ],


            'horário',
            'local',
            ['attribute'=>'id_usuario.nome','label'=>'Usuário'],
            'descricao',
            //'id_cons',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
