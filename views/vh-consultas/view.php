<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\VhConsultas */

$this->title = $model->id_usuario;
$this->params['breadcrumbs'][] = ['label' => 'Consultas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="vh-consultas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Atualizar', ['update', 'id_usuario' => $model->id_usuario, 'id_cons' => $model->id_cons], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Apagar', ['delete', 'id_usuario' => $model->id_usuario, 'id_cons' => $model->id_cons], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Tem certeza de que deseja excluir este item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'dia',
            'horário',
            'local',
            'descricao',

            ['attribute'=>'usuario.nome', 'label'=>'Codigo do Usuário'],
            'id_cons'

        ],
    ]) ?>

</div>
