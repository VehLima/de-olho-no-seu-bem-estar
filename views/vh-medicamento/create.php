<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\VhMedicamento */

$this->title = 'Adicionar Medicamento';
$this->params['breadcrumbs'][] = ['label' => 'Medicamentos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vh-medicamento-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
