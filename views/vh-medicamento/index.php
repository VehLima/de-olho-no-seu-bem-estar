<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\VhMedicamentoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = ' Medicamentos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vh-medicamento-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Adicionar Medicamento', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'nome',
            ['attribute'=>'id_medic.nome','label'=>'Medicamento'],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
