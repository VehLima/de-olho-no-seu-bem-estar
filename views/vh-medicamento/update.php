<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\VhMedicamento */

$this->title = 'Atualizar Medicamento: ' . $model->id_medic;
$this->params['breadcrumbs'][] = ['label' => 'Medicamentos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_medic, 'url' => ['view', 'id' => $model->id_medic]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="vh-medicamento-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
