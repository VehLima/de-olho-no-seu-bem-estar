<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vh_usuario".
 *
 * @property string $nome
 * @property string $email
 * @property string $sexo
 * @property int $id_usuario
 *
 * @property VhAlimentacao[] $vhAlimentacaos
 * @property VhConsultas[] $vhConsultas
 * @property VhHmed[] $vhHmeds
 */
class VhUsuario extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vh_usuario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome', 'email'], 'string', 'max' => 150],
            [['sexo'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nome' => 'Nome',
            'email' => 'E-mail',
            'sexo' => 'Sexo',
            'id_usuario' => 'Código do  Usuário',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVhAlimentacaos()
    {
        return $this->hasMany(VhAlimentacao::className(), ['id_usuario' => 'id_usuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVhConsultas()
    {
        return $this->hasMany(VhConsultas::className(), ['id_usuario' => 'id_usuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVhHmeds()
    {
        return $this->hasMany(VhHmed::className(), ['id_usuario' => 'id_usuario']);
    }
}
