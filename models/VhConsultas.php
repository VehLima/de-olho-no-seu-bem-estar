<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vh_consultas".
 *
 * @property string $dia
 * @property string $horário
 * @property string $local
 * @property int $id_usuario
 * @property string $descricao
 * @property int $id_cons
 *
 * @property VhUsuario $usuario
 */
class VhConsultas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vh_consultas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dia'], 'safe'],
            [['horário'], 'time'],
            [['id_usuario'], 'required'],
            [['id_usuario'], 'integer'],
            [['local'], 'string', 'max' => 150],
            [['descricao'], 'string', 'max' => 200],
            [['id_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => VhUsuario::className(), 'targetAttribute' => ['id_usuario' => 'id_usuario']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dia' => 'Dia',
            'horário' => 'Horário',
            'local' => 'Local',
            'id_usuario' => 'Código do Usuário',
            'descricao' => 'Descrição',
            'id_cons' => 'Código da Consulta',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(VhUsuario::className(), ['id_usuario' => 'id_usuario']);
    }
}
