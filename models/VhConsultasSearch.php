<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\VhConsultas;

/**
 * VhConsultasSearch represents the model behind the search form of `app\models\VhConsultas`.
 */
class VhConsultasSearch extends VhConsultas
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dia', 'horário'], 'number'],
            [['local', 'descricao','usuario.nome'], 'safe'],
            [[], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VhConsultas::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

         
        $query->joinWith(['usuario']);
        $dataProvider->sort->attributes['usuario.nome']=[
            'asc'=>['usuario.nome'=>SORT_ASC],
            'desc'=>['usuario.nome'=>SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'dia' => $this->dia,
            'horário' => $this->horário,
            'id_usuario' => $this->id_usuario,
            'id_cons' => $this->id_cons,
        ]);

        $query->andFilterWhere(['LIKE', 'usuario.nome',$this->getAttribute('usuario.nome')]);


        return $dataProvider;
    }
}
