<?php

namespace app\models;

use Yii;
use app\models\VhAlimentacao;
use app\models\VhAlimentacaoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * VhAlimentacaoController implements the CRUD actions for VhAlimentacao model.
 */
class VhAlimentacaoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all VhAlimentacao models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VhAlimentacaoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single VhAlimentacao model.
     * @param integer $id_usuario
     * @param integer $id_aliment
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id_usuario, $id_aliment)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_usuario, $id_aliment),
        ]);
    }

    /**
     * Creates a new VhAlimentacao model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new VhAlimentacao();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_usuario' => $model->id_usuario, 'id_aliment' => $model->id_aliment]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing VhAlimentacao model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id_usuario
     * @param integer $id_aliment
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id_usuario, $id_aliment)
    {
        $model = $this->findModel($id_usuario, $id_aliment);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_usuario' => $model->id_usuario, 'id_aliment' => $model->id_aliment]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing VhAlimentacao model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id_usuario
     * @param integer $id_aliment
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id_usuario, $id_aliment)
    {
        $this->findModel($id_usuario, $id_aliment)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the VhAlimentacao model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id_usuario
     * @param integer $id_aliment
     * @return VhAlimentacao the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_usuario, $id_aliment)
    {
        if (($model = VhAlimentacao::findOne(['id_usuario' => $id_usuario, 'id_aliment' => $id_aliment])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
