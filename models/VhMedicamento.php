<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vh_medicamento".
 *
 * @property string $nome
 * @property int $id_medic
 */
class VhMedicamento extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vh_medicamento';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nome' => 'Nome',
            'id_medic' => 'Código do Medicamento',
        ];
    }
}
