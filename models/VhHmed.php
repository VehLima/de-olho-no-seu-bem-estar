<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vh_hmed".
 *
 * @property int $id_usuario
 * @property string $descricao
 * @property int $id_cons
 *
 * @property VhUsuario $usuario
 */
class VhHmed extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vh_hmed';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_usuario'], 'required'],
            [['id_usuario'], 'integer'],
            [['descricao'], 'string', 'max' => 200],
            [['id_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => VhUsuario::className(), 'targetAttribute' => ['id_usuario' => 'id_usuario']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_usuario' => 'Código do Usuário',
            'descricao' => 'Descrição',
            'id_cons' => 'Código da Consulta',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(VhUsuario::className(), ['id_usuario' => 'id_usuario']);
    }
}
