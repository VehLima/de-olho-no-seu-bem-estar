<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vh_alimentacao".
 *
 * @property string $horário
 * @property string $descricao
 * @property int $id_usuario
 * @property int $id_aliment
 *
 * @property VhUsuario $usuario
 */
class VhAlimentacao extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vh_alimentacao';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['horário'], 'time'],
            [['id_usuario'], 'required'],
            [['id_usuario'], 'integer'],
            [['descricao'], 'string', 'max' => 200],
            [['id_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => VhUsuario::className(), 'targetAttribute' => ['id_usuario' => 'id_usuario']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'horário' => 'Horário',
            'descricao' => 'Descrição',
            'id_usuario' => 'Código do Usuário',
            'id_aliment' => 'Código da Alimentação',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(VhUsuario::className(), ['id_usuario' => 'id_usuario']);
    }
}
