create database trabalhofinal_vh;
use  trabalhofinal_vh;

create table vh_usuario(
nome varchar(150),
email varchar(150),
sexo varchar(15),
id_usuario int auto_increment,
primary key(id_usuario)
);

create table vh_medicamento(
nome varchar(100),
id_medic int auto_increment,
primary key(id_medic)
);
 
 create table vh_alimentacao(
horário numeric,
descricao varchar(200),
id_usuario int,
id_aliment int auto_increment,
primary key(id_aliment,id_usuario),
foreign key(id_usuario) references vh_usuario(id_usuario)
);

 create table vh_consultas(
dia numeric,
horário numeric,
local varchar(150),
id_usuario int,
descricao varchar(200),
id_cons int auto_increment,
primary key(id_cons,id_usuario),
foreign key(id_usuario) references vh_usuario(id_usuario)
);

 create table vh_hmed(
id_usuario int,
descricao varchar(200),
id_cons int auto_increment,
primary key(id_cons,id_usuario),
foreign key(id_usuario) references vh_usuario(id_usuario)
);

